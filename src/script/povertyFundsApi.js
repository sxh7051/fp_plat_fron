import axios from 'axios';

// 封装axios请求为api对象  
const povertyFundsApi = {

    /**
     * @param word
     * @param current
     * @param pageSize
     * @returns {Promise<fp_Pagination<fp_PovertyFunds>>}
     */
    // 搜索分页数据
    async searchList(word = '', current = 1, pageSize = 10) {
        const response = await axios.get(`/poverty_funds/search`, {params: {word, current, pageSize}});
        return response.data;
    },


    /**
     * @param current
     * @param pageSize
     * @returns {Promise<fp_Pagination<fp_PovertyFunds>>}
     */
    async list(current = 1, pageSize = 10) {
        const response = await axios.get(`/poverty_funds/?current=${current}&pageSize=${pageSize}`);
        return response.data  
    },


    /**
     * @returns {Promise<fp_PovertyFunds>}
     */
    async getById(id) {
        const response = await axios.get(`/poverty_funds/${id}`);
        return response.data
    },

    async create(params) {
        const response = await axios.post('/poverty_funds/create', params);
        return response.data
    },

    async delete(id) {
        const response = await axios.post(`/poverty_funds/delete/${id}`);
        return response.data
    },

    async update(params) {
        const response = await axios.post('/poverty_funds/update', params);
        return response.data
    }
};

export default povertyFundsApi;