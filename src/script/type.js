/**
 * Result包装类，用于封装操作结果，包含消息、代码和数据。
 * @template T
 * @typedef {Object} fp_Result<T>
 * @property {string} msg - 操作的消息描述。
 * @property {number} code - 操作的返回代码。
 * @property {T} data - 操作返回的数据。
 */

/**
 * 分页类
 * @template T
 * @typedef {Object} fp_Pagination<T>
 * @property {T[]} records - 用户记录列表
 * @property {number} total - 总记录数
 * @property {number} size - 每页记录数
 * @property {number} current - 当前页码
 * @property {number} pages - 总页数
 */

/**
 * 用户记录类
 * @typedef {Object} fp_Admin
 * @property {number} id - 用户ID
 * @property {string} username - 用户名
 * @property {string} password - 加密后的密码
 * @property {string} realName - 真实姓名
 * @property {string} role - 角色
 * @property {string} email - 邮箱地址
 * @property {string} phone - 电话号码
 * @property {string} createdAt - 创建时间
 * @property {string} updatedAt - 更新时间
 * @property {boolean} isActive - 是否激活状态
 */

/**
 * 扶贫项目对象
 *
 * @typedef {Object} fp_PovertyAlleviationProject
 * @property {number} id - 项目的唯一标识符
 * @property {string} projectName - 项目的名称
 * @property {string} projectType - 项目的类型，例如："教育"、"农业"、"医疗"等
 * @property {number} budget - 项目的预算金额
 * @property {string} startDate - 项目的开始日期，格式为ISO 8601
 * @property {string} endDate - 项目的结束日期，格式为ISO 8601
 * @property {string} status - 项目的状态，例如："进行中"、"已完成"、"已取消"等
 */

/**
 * 扶贫资金对象
 *
 * @typedef {Object} fp_PovertyFunds
 * @property {number} id - 资金的唯一标识符
 * @property {number} projectId - 对应的项目ID
 * @property {number} budget - 预算金额
 * @property {number} usedAmount - 已使用金额
 * @property {number} remainingAmount - 剩余金额
 * @property {string} usageDescription - 资金使用描述
 */

