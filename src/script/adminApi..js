import axios from 'axios';

const AdminApi = {
    /**
     * @param {string} name
     * @param {string} password
     * @returns {Promise<fp_Result<any>>}
     */
    // 管理员登录
    async login(name = 'no', password = 'no') {
        const response = await axios.get(`/poverty_admin/login`, {params: {name, password}});
        return response.data;
    },

    /**
     *
     * @param word
     * @param current
     * @param pageSize
     * @returns {Promise<fp_Result<fp_Pagination<fp_Admin>>>}
     */
    // 搜索管理员列表的分页数据
    async searchList(word = '', current = 1, pageSize = 10) {
        const response = await axios.get(`/poverty_admin/search`, {params: {word, current, pageSize}});
        return response.data;
    },

    /**
     * @param current
     * @param pageSize
     * @returns {Promise<fp_Result<fp_Pagination<fp_Admin>>>}
     */
    // 获取管理员列表的分页数据
    async list(current = 1, pageSize = 10) {
        const response = await axios.get(`/poverty_admin/`, {params: {current, pageSize}});
        return response.data;
    },

    /**
     *
     * @param id
     * @returns {Promise<fp_Result<fp_Admin>>}
     */
    // 根据ID获取管理员信息
    async getById(id) {
        const response = await axios.get(`/poverty_admin/${id}`);
        return response.data;
    },

    /**
     *
     * @param {fp_Admin} params
     * @returns {Promise<any>}
     */
    // 创建管理员
    async create(params) {
        const response = await axios.post('/poverty_admin/create', params);
        return response.data;
    },

    // 删除管理员
    async delete(id) {
        const response = await axios.post(`/poverty_admin/delete/${id}`);
        return response.data;
    },

    /**
     *
     * @param {fp_Admin} params
     * @returns {Promise<any>}
     */
    // 更新管理员
    async update(params) {
        const response = await axios.post('/poverty_admin/update', params);
        return response.data;
    }
};

export default AdminApi;