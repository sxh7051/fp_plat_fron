import axios from 'axios';

const povertyAlleviationProjectsApi = {

    /**
     * @param word
     * @param current
     * @param pageSize
     * @returns {Promise<fp_Pagination<fp_PovertyAlleviationProject>>}
     */
    // 搜索分页数据
    async searchList(word = '', current = 1, pageSize = 10) {
        const response = await axios.get(`/poverty_alleviation_projects/search`, {params: {word, current, pageSize}});
        return response.data;
    },

    /**
     *
     * @param current
     * @param pageSize
     * @returns {Promise<fp_Pagination<fp_PovertyAlleviationProject>>}
     */
    // 列出扶贫项目
    list: async (current = 1, pageSize = 10) => {
        const response = await axios.get(`/poverty_alleviation_projects/`, {
            params: {
                current,
                pageSize
            }
        });
        return response.data;
    },

    /**
     *
     * @param id
     * @returns {Promise<fp_PovertyAlleviationProject>}
     */
    // 获取指定id的扶贫项目  
    getById: async (id) => {
        const response = await axios.get(`/poverty_alleviation_projects/${id}`);
        return response.data;
    },

    /**
     *
     * @param {fp_PovertyAlleviationProject} params
     * @returns {Promise<any>}
     */
    // 创建扶贫项目  
    create: async (params) => {
        const response = await axios.post('/poverty_alleviation_projects/create', params);
        return response.data;
    },

    // 删除指定id的扶贫项目  
    delete: async (id) => {
        const response = await axios.post(`/poverty_alleviation_projects/delete/${id}`);
        return response.data;
    },

    // 更新扶贫项目  
    update: async (params) => {
        const response = await axios.post('/poverty_alleviation_projects/update', params);
        return response.data;
    }
};

export default povertyAlleviationProjectsApi;