import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from "@/views/HomeView.vue";
import PovertyAdminView from "@/views/PovertyAdminView.vue";
import PovertyNeedsView from "@/views/PovertyNeedsView.vue";
import PovertyAlleviationProjectsView from "@/views/PovertyAlleviationProjectsView.vue";
import PovertyHouseholdsView from "@/views/PovertyHouseholdsView.vue";
import PovertyFundsView from "@/views/PovertyFundsView.vue";
import PovertyAssistanceView from "@/views/PovertyAssistanceView.vue";
import LoginView from "@/views/LoginView.vue";
import LayoutView from "@/views/LayoutView.vue";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/login',
        name: 'login',
        component: LoginView
    },
    {
        path: '/',
        name: 'layout',
        component: LayoutView,
        children: [
            {
                path: '/home',
                name: 'home',
                component: HomeView
            },
            {
                path: '/poverty_households',
                component: PovertyHouseholdsView,
                meta: {
                    title: "贫困户基本信息",
                    负责人: "韩飞墨",
                }
            },
            {
                path: '/poverty_needs',
                component: PovertyNeedsView,
                meta: {
                    title: "贫困户需求情况",
                    负责人: "王佳峰",
                }
            },
            {
                path: '/poverty_alleviation_projects',
                component: PovertyAlleviationProjectsView,
                meta: {
                    title: "扶贫项目",
                    负责人: "苏欣昊",
                }
            },
            {
                path: '/poverty_funds',
                component: PovertyFundsView,
                meta: {
                    title: "扶贫资金使用情况",
                    负责人: "苏欣昊",
                }
            },
            {
                path: '/poverty_assistance',
                component: PovertyAssistanceView,
                meta: {
                    title: "贫困户帮扶情况",
                    负责人: "武琦",
                }
            },
            {
                path: "/poverty_admin",
                component: PovertyAdminView,
                meta: {
                    title: "管理员信息",
                    负责人: "韩飞墨",
                }
            }
        ]
    }


]

const router = new VueRouter({
    routes
})
router.beforeEach((to,from,next) => {
    const admin = localStorage.getItem("user");
    if(!admin && to.path !='/login'){
        return next("/login");
    }
    next();
})

export default router
